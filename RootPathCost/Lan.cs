using System.Collections.Generic;

namespace RootPathCost
{
    class Lan
    {
        public string Name { get; private set; }
        public IList<PortConn> PortConns { get { return portConns.AsReadOnly(); } }
        private List<PortConn> portConns;

        public Lan(string name)
        {
            Name = name;
            portConns = new List<PortConn>();
        }

        public void AddPort(PortConn port)
        {
            portConns.Add(port);
            port.Lan = this;
        }
    }
}