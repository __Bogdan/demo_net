using System;
using System.Collections.Generic;
using System.Linq;
using MoreLinq;

namespace RootPathCost
{
    /// <summary>
    /// Represents a network
    /// </summary>
    class Network
    {

        /// <summary>
        /// Allows to view MaxRootPath
        /// </summary>
        public uint MaxRootPath
        {
            get
            {
                return GetRootPathes().Values.Max();
            }
        }

        /// <summary>
        /// Allows to view list of bridges without of rights to edit it.
        /// </summary>
        public IList<Bridge> Bridges { get { return bridges.AsReadOnly(); } }
        /// <summary>
        /// Allows to view list of lans without of rights to edit it.
        /// </summary>
        public IList<Lan> Lans { get { return lans.AsReadOnly(); } }
        /// <summary>
        /// Stores root bridge of network
        /// </summary>
        public Bridge Root { get; private set; }

        /// <summary>
        /// Creates new instance of network
        /// </summary>
        public Network()
        {
            bridges = new List<Bridge>();
            lans = new List<Lan>();
            Root = null;
        }

        /// <summary>
        /// Adds new bridge to network
        /// </summary>
        /// <param name="bridge"></param>
        public void AddBridge(Bridge bridge)
        {

            if (bridges.Exists(b => b.Name == bridge.Name))
            {
                throw new Exception(
                    String.Format("Repeated bridge name '{0}'", bridge.Name));
            }

            bridges.Add(bridge);
            Root = bridges.MinBy(b => b.BridgeId);

        }

        /// <summary>
        /// Allows to create lan with unique names only
        /// </summary>
        /// <param name="name">Name of lan to create</param>
        /// <returns>New lan object</returns>
        public Lan CreateLan(string name)
        {
            Lan lan = lans.FirstOrDefault(l => l.Name == name);
            if (lan == null)
            {
                lan = new Lan(name);
                lans.Add(lan);
            }
            return lan;
        }

        /// <summary>
        /// Stores list of bridges
        /// </summary>
        private List<Bridge> bridges;

        /// <summary>
        /// Gets root pathes of all bridges in network using Dijkstra's algorithm
        /// </summary>
        /// <returns>Dictionary with info about root path of every bridge</returns>
        private IDictionary<Bridge, uint> GetRootPathes()
        {
            Bridge currBridge = Root;
            IDictionary<Bridge, uint> pathes = new Dictionary<Bridge, uint>();
            foreach (Bridge bridge in bridges)
            {
                pathes[bridge] = uint.MaxValue;
            }
            pathes[currBridge] = 0;

            List<Bridge> bridgesToPass = new List<Bridge>(bridges);

            while (bridgesToPass.Count > 1)
            { // 1 - because just after next line count will be '0'

                bridgesToPass.Remove(currBridge);

                //try to reach all neighbors
                foreach (PortConn port in currBridge.PortConns)
                {
                    foreach (PortConn neighborPort in port.Lan.PortConns)
                    {
                        if (bridgesToPass.Contains(neighborPort.Bridge))
                        {
                            uint neighborCosts = pathes[currBridge] + neighborPort.PathCost;
                            uint prevNeighborCosts = pathes[neighborPort.Bridge];
                            pathes[neighborPort.Bridge] = prevNeighborCosts < neighborCosts ? prevNeighborCosts
                                : neighborCosts;
                        }
                    }
                }

                //pick next bridge
                Bridge nextBridge = null;
                uint minCost = uint.MaxValue;
                foreach (Bridge b in bridgesToPass)
                {
                    if (pathes[b] < minCost)
                    {
                        nextBridge = b;
                        minCost = pathes[b];
                    }
                }
                currBridge = nextBridge;
            }
            return pathes;
        }

        /// <summary>
        /// Stores list of lans
        /// </summary>
        private List<Lan> lans;

    }
}
