﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RootPathCost
{
    class PortConn
    {
        public Bridge Bridge
        {
            get { return bridge; }
            set
            {
                if (bridge == null)
                {
                    bridge = value;
                }
                else
                {
                    throw new Exception(String.Format("The port '{0}' already has parent '{1}', though '{2} was being set'",
                    this.PortNum, this.Bridge.Name, value.Name));
                }
            }
        }
        private Bridge bridge;
        public uint PathCost { get; private set; }
        public Lan Lan
        {
            get
            {
                return _lan;
            }
            set
            {
                if (_lan == null)
                {
                    _lan = value;
                }
                else
                {
                    throw new Exception(
                        String.Format("Bridge '{0}', port '{1}: trying to set new lan '{2}, old is '{3}'",
                        bridge.Name, this.PortNum, value.Name, _lan.Name)
                    );
                }
            }
        }

        private Lan _lan;

        public ushort PortNum { get; private set; }


        public PortConn(uint pathCost, ushort portNum)
        {
            PathCost = pathCost;
            PortNum = portNum;
            bridge = null;
            _lan = null;
        }
    }
}
