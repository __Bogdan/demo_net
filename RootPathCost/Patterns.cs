namespace RootPathCost
{
    static class Patterns
    {
        public const string BridgeHead = @"^\[([ a-zA-Z0-9]+)\]$";
        public const string BridgeId = @"^BridgeId = (\d+)$";
        public const string BridgePort = @"^\[([ a-zA-Z0-9]+)\.\s*Port (\d+)\]$";
        public const string PathCost = @"^PathCost = (\d+)$";
        public const string ConnectedTo = @"^ConnectedTo = ([ a-zA-Z0-9]+)$";
        public const string Empty = @"^\s*$";
    }
}