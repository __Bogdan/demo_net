﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RootPathCost
{
    class Bridge
    {
        public string Name { get; private set; }
        public uint BridgeId { get; private set; }
        public IList<PortConn> PortConns
        {
            get
            {
                return portConns.AsReadOnly();
            }
        }
        private List<PortConn> portConns { get; set; }

        public Bridge(string name, uint bridgeId)
        {
            Name = name;
            BridgeId = bridgeId;
            portConns = new List<PortConn>();
        }

        public void AddPort(PortConn port)
        {

            if (portConns.Exists(p => p.PortNum == port.PortNum))
            {
                throw new Exception(String.Format("duplicate port '{0}' on bridge '{1}'", port.PortNum, this.Name));
            }

            port.Bridge = this;
            portConns.Add(port);
        }
    }
}
