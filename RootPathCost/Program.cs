﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace RootPathCost
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> inputs = new List<string>();

            try
            {
                if (args.Length > 0)
                {
                    string fileName = args[0];
                    FileInfo finfo = new FileInfo(fileName);

                    if (finfo.Length > (1 << 20))
                    {
                        throw new IOException("Too big file");
                    }

                    string[] content = File.ReadAllLines(fileName);
                    inputs.AddRange(content);

                }
                else
                {
                    do
                    {
                        string inputLine = Console.ReadLine();
                        if (inputLine == "end") break;
                        inputs.Add(inputLine);
                    } while (true);
                }

                NetworkValidator validator = new NetworkValidator();
                Network network = validator.CreateNetwork(inputs);
                uint maxRootPath = network.MaxRootPath;

                Console.WriteLine("Max root path = {0}", maxRootPath);
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
        }
    }
}

