﻿
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace RootPathCost
{

    class NetworkValidator
    {
        public string LastPattern { get; private set; }
        string[] currPatterns;
        Dictionary<string, string[]> dictionary;

        public NetworkValidator()
        {
            this.dictionary = new Dictionary<string, string[]>();
            dictionary[Patterns.BridgeHead] = new string[] { Patterns.BridgeId };
            dictionary[Patterns.BridgeId] = new string[] { Patterns.BridgePort };
            dictionary[Patterns.BridgePort] = new string[] { Patterns.PathCost };
            dictionary[Patterns.PathCost] = new string[] { Patterns.ConnectedTo };
            dictionary[Patterns.ConnectedTo] = new string[] { Patterns.Empty, Patterns.BridgePort};
            dictionary[Patterns.Empty] = new string[] { Patterns.BridgeHead };

            this.currPatterns = dictionary[Patterns.Empty];
            
        }

        GroupCollection Check(string line)
        {

            bool res = false;
            Match match = null;

            foreach (string pattern in currPatterns)
            {
                match = Regex.Match(line, pattern);
                if (match.Success)
                {
                    res = true;
                    currPatterns = dictionary[pattern];
                    LastPattern = pattern;
                    break;
                }
            }

            if (!res)
            {
                throw new Exception(
                    String.Format(
                        "Line '{0}' is invalid: '{1}' expected", line, String.Join(" or ", currPatterns)));
            }

            return match.Groups;
        }

        public Network CreateNetwork(List<string> inputs)
        {
            // Ensure the inputs are terminated
            inputs.Add("");

            Network network = new Network();

            string lastBridgeName = null;
            Bridge lastBridge = null;
            Bridge lastParentBridge = null;

            uint lastPathCost = 0;
            ushort lastPortNum = 0;
            string lastLan = null;


            foreach (string line in inputs)
            {
                GroupCollection vals = Check(line);


                switch (LastPattern)
                {
                    case Patterns.BridgeHead:
                        lastBridgeName = vals[1].Value;
                        break;

                    case Patterns.BridgeId:
                        uint bridgeId = uint.Parse(vals[1].Value);
                        lastBridge = new Bridge(lastBridgeName, bridgeId);
                        break;

                    case Patterns.BridgePort:
                        string bridgeName = vals[1].Value;
                        lastPortNum = ushort.Parse(vals[2].Value);

                        Bridge parentBridge = null;
                        if (bridgeName == lastBridgeName)
                        {
                            parentBridge = lastBridge;
                        }
                        else
                        {
                            throw new Exception(
                                String.Format("Not a parent bridge: {0} instead {1}", lastBridge.Name, bridgeName));
                        }

                        lastParentBridge = parentBridge;
                        break;

                    case Patterns.PathCost:
                        lastPathCost = uint.Parse(vals[1].Value);
                        break;

                    case Patterns.ConnectedTo:
                        lastLan = vals[1].Value;
                        Lan lan = network.CreateLan(lastLan);
                        PortConn port = new PortConn(lastPathCost, lastPortNum);
                        lan.AddPort(port);
                        lastBridge.AddPort(port);
                        break;

                    case Patterns.Empty:
                        network.AddBridge(lastBridge);
                        break;
                }
            }


            return network;
        }
    }
}